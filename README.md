# Anserini Indexes

This repository holds pre-built indexes  be used with [Anserini](https://github.com/castorini/anserini) or [Pyserini](https://github.com/castorini/pyserini).


```bash
$ wget https://git.uwaterloo.ca/m2abuals/indexes/-/raw/master/athome_sample_index.tar.gz
```


